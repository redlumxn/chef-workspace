# CHANGELOG for install-rvm

This file is used to list changes made in each version of install-rvm.

## 0.1.0:

* Initial release of install-rvm

- - -
Check the [Markdown Syntax Guide](http://daringfireball.net/projects/markdown/syntax) for help with Markdown.

The [Github Flavored Markdown page](http://github.github.com/github-flavored-markdown/) describes the differences between markdown on github and standard markdown.
