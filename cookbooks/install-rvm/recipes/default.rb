#
# Cookbook Name:: install-rvm
# Recipe:: default
#
# Copyright 2013, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#
package "curl" do
  action :install
  options "--force-yes"
end

execute "download-rvm-script" do
  command "curl -L https://get.rvm.io > #{node['rvm_temp_script_filename']}"
  user "#{node['user']}"
  cwd "/home/#{node['user']}"
  environment ({'HOME' => "/home/#{node['user']}"})
  not_if "test -d /home/#{node['user']}/.rvm"
  action :run
end

execute "change-permissions-rvm-script" do
  command "chmod +x #{node['rvm_temp_script_filename']}"
  user "redlumxn"
  cwd "/home/#{node['user']}"
  environment ({'HOME' => "/home/#{node['user']}"})
  not_if "test -d /home/#{node['user']}/.rvm"
  action :run
end

execute "execute-rvm-script" do
  command "bash #{node['rvm_temp_script_filename']} --version latest stable"
  user "redlumxn"
  cwd "/home/#{node['user']}"
  environment ({'HOME' => "/home/#{node['user']}"})
  not_if "test -d /home/#{node['user']}/.rvm"
  action :run
end

execute "delete-rvm-script" do
  command "rm #{node['rvm_temp_script_filename']}"
  user "redlumxn"
  cwd "/home/#{node['user']}"
  environment ({'HOME' => "/home/#{node['user']}"})
  only_if "test -f /home/#{node['user']}/#{node['rvm_temp_script_filename']}"
  action :run
end
