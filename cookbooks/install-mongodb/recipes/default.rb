#
# Cookbook Name:: install-mongodb
# Recipe:: default
#
# Copyright 2013, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute

execute "import-mongodb-gpg_key" do
  command "sudo apt-key adv --keyserver keyserver.ubuntu.com --recv 7F0CEB10"
  not_if "test -f /etc/apt/sources.list.d/10gen.list"
end

template "/etc/apt/sources.list.d/10gen.list" do
  source "10gen.list.erb"
  not_if "test -f /etc/apt/sources.list.d/10gen.list"
end

execute "upload repository" do
  command "sudo apt-get update"
  not_if "test -f /etc/mongodb.conf"
end

package "mongodb-10gen" do
  not_if "test -f /etc/mongodb.conf"
  action :install
end

#
