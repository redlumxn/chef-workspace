#
# Cookbook Name:: setup-users
# Recipe:: default
#
# Copyright 2013, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#
user "redlumxn" do
  comment "My prefered user"
  gid "users"
  home "/home/redlumxn"
  shell "/bin/bash"
  password "$1$H3huD8MN$6vDrUbKkVYdvZ1Ib4yJ64/"
  supports :manage_home => true 
end

execute "usermod" do
  command "usermod -a -G admin redlumxn"
  action :run
end
